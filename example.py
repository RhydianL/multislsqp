
from multislsqp import minimise_slsqp
import numpy as np

np.random.seed(100)

'''
Example scripts to find the minimisation points of the sum of sin function over a variety of dimensions
'''

def fnN(x):
    f = np.sin(x)
    if np.array(x).ndim == 2: fsum = f.sum(1)
    else: fsum = f.sum()
    return fsum

def dfnN(x):
    df = np.cos(x)
    return df

def fnN_all(x):
    return fnN(x),dfnN(x)

def main():
    dim = 3 # dimension of problem
    m = 10 # number of different starting points
    x = np.random.uniform(-5,5,size=(m,dim))
    results = minimise_slsqp(fnN_all,x,jac=True)
    for i,result in enumerate(results):
        print('#######################################')
        print('Results from starting point {}'.format(x[i]))
        print()
        print(result)
        print()

if __name__=='__main__':
    main()