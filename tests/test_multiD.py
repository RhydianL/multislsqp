import numpy as np
from multislsqp import scipy_version
from multislsqp.utils import runner,create_x_seed as create_x

# ====================================================================
# ND functions
def fnN(x):
    f = np.sin(x)
    if np.array(x).ndim == 2: fsum = f.sum(1)
    else: fsum = f.sum()
    return fsum

def dfnN(x):
    df = np.cos(x)
    return df

def fnN_all(x):
    return fnN(x),dfnN(x)

def constrN(x):
    return fnN(x) - 0.8

def dconstrN(x):
    return dfnN(x)

def _testND_jac_combined(N=5,m=10,bounds=None,ix=None,**kwargs):
    x_range = [(-3,3)]*N
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range
    f_dict = {'fun':fnN_all,'jac':True}
    same = runner(x,f_dict,bounds=bounds,**kwargs)
    assert same==True

def _testND_jac_separate(N=5,m=10,bounds=None,ix=None,**kwargs):
    x_range = [(-3,3)]*N
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range

    f_dict = {'fun':fnN,'jac':dfnN}
    same = runner(x,f_dict,bounds=bounds,**kwargs)
    assert same==True

def _testND_jac_approx(N=5,m=10,bounds=None,ix=None,method=False,**kwargs):
    x_range = [(-3,3)]*N
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range
    # x = x[:1]
    f_dict = {'fun':fnN,'jac':method}
    same = runner(x,f_dict,bounds=bounds,**kwargs)
    assert same==True

def _testND_const_eq(N=5,m=10,bounds=None,ix=None,**kwargs):
    ''' test out behaviour with inequality contraint'''
    x_range = [(-3,3)]*N
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range
    f_dict = {'fun':fnN_all,'jac':True}
    # x = x[2]
    con = {'type': 'eq', 'fun': constrN, 'jac':dconstrN}
    same = runner(x,f_dict,bounds=bounds,constraints=con,**kwargs)
    assert same==True

def _testND_const_ineq(N=5,m=10,bounds=None,ix=None,**kwargs):
    ''' test out behaviour with inequality contraint'''
    x_range = [(-3,3)]*N
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range
    f_dict = {'fun':fnN_all,'jac':True}
    con = {'type': 'ineq', 'fun': constrN, 'jac':dconstrN}
    same = runner(x,f_dict,bounds=bounds,constraints=con,**kwargs)
    assert same==True

# test out scenario where gradient is provided with function
def testND_jac_combined(*args,**kwargs):
    _testND_jac_combined(*args,**kwargs)

def testND_jac_combined_bounds(*args,**kwargs):
    _testND_jac_combined(*args,bounds=True,**kwargs)

def testND_jac_combined_single(*args,**kwargs):
    _testND_jac_combined(*args,ix=0,**kwargs)

# test out scenario where gradient is provided separately
def testND_jac_separate(*args,**kwargs):
    _testND_jac_separate(*args,**kwargs)

def test1D_jac_separate_bounds(*args,**kwargs):
    _testND_jac_separate(*args,bounds=True,**kwargs)

def test1D_jac_separate_single(*args,**kwargs):
    _testND_jac_separate(*args,ix=0,**kwargs)

# test out scenario where gradient is approximated (using FD)
def testND_jac_approx(*args,**kwargs):
    _testND_jac_approx(*args,**kwargs)

def testND_jac_approx_bounds(*args,**kwargs):
    _testND_jac_approx(*args,bounds=True,**kwargs)

def testND_jac_approx_single(*args,**kwargs):
    _testND_jac_approx(*args,ix=0,**kwargs)

if scipy_version[1]>5:
    def testND_jac_approx_2point(*args,**kwargs):
        _testND_jac_approx(*args,method='2-point',**kwargs)

    def testND_jac_approx_3point(*args,**kwargs):
        _testND_jac_approx(*args,method='3-point',**kwargs)

    def testND_jac_approx_cs(*args,**kwargs):
        _testND_jac_approx(*args,method='cs',**kwargs)

# test out scenario where equality constraint is given 
def testND_const_eq(*args,**kwargs):
    _testND_const_eq(*args,**kwargs)

def testND_const_eq_bounds(*args,**kwargs):
    _testND_const_eq(*args,bounds=True,**kwargs)

def testND_const_eq_single(*args,**kwargs):
    _testND_const_eq(*args,ix=0,**kwargs)    

# test out scenario where inequality constraint is given 
def testND_const_ineq(*args,**kwargs):
    _testND_const_ineq(*args,**kwargs)

def testND_const_ineq_bounds(*args,**kwargs):
    _testND_const_ineq(*args,bounds=True,**kwargs)

def testND_const_ineq_single(*args,**kwargs):
    _testND_const_ineq(*args,ix=0,**kwargs)        









