import numpy as np
from multislsqp import scipy_version
from multislsqp.utils import runner,create_x_seed as create_x

# ====================================================================
# 1D function

def fn1(x):
    f = np.sin(x)
    return f

def dfn1(x):
    df = np.cos(x)
    return df

def fn1_all(x):
    return fn1(x), dfn1(x)

def constr1(x):
    return np.sin(x) - 0.8

def dconstr1(x):
    return np.cos(x)

# ====================================================================
# tests

def _test1D_jac_combined(m=10,bounds=None,ix=None,**kwargs):
    ''' test out behaviour when function and evaluation are given together'''
    x_range = [(-10,10)]
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range

    f_dict = {'fun':fn1_all,'jac':True}

    same = runner(x,f_dict,bounds=bounds,**kwargs)
    assert same==True

def _test1D_jac_separate(m=10,bounds=None,ix=None,**kwargs):
    ''' test out behaviour when function and evaluation are given separately'''
    x_range = [(-10,10)]
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range

    f_dict = {'fun':fn1,'jac':dfn1}
    same = runner(x,f_dict,bounds=bounds,**kwargs)
    assert same==True

def _test1D_jac_approx(m=10,bounds=None,ix=None,method=False,**kwargs):
    ''' test out behaviour when jacobian is approximated'''
    x_range = [(-10,10)]
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range

    f_dict = {'fun':fn1,'jac':method}
    same = runner(x,f_dict,bounds=bounds,**kwargs)
    assert same==True

def _test1D_const_eq(m=10,bounds=None,ix=None,**kwargs):
    ''' test out behaviour with equality contraint'''
    x_range = [(-10,10)]
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range
    # x = x[4]
    f_dict = {'fun':fn1_all}
    con = {'type': 'eq', 'fun': constr1, 'jac':dconstr1}
    same = runner(x,f_dict,bounds=bounds,constraints=con,**kwargs)
    assert same==True

def _test1D_const_ineq(m=10,bounds=None,ix=None,**kwargs):
    ''' test out behaviour with inequality contraint'''
    x_range = [(-10,10)]
    x = create_x(m,x_range)
    if ix:
        x = x[ix]
    if bounds:
        bounds=x_range
    f_dict = {'fun':fn1_all}
    con = {'type': 'ineq', 'fun': constr1, 'jac':dconstr1}
    same = runner(x,f_dict,bounds=bounds,constraints=con,**kwargs)
    assert same==True

# test out scenario where gradient is provided with function
def test1D_jac_combined(*args,**kwargs):
    _test1D_jac_combined(*args,**kwargs)

def test1D_jac_combined_bounds(*args,**kwargs):
    _test1D_jac_combined(*args,bounds=True,**kwargs)

def test1D_jac_combined_single(*args,**kwargs):
    _test1D_jac_combined(*args,ix=0,**kwargs)

# test out scenario where gradient is provided separately
def test1D_jac_separate(*args,**kwargs):
    _test1D_jac_separate(*args,**kwargs)

def test1D_jac_separate_bounds(*args,**kwargs):
    _test1D_jac_separate(*args,bounds=True,**kwargs)

def test1D_jac_separate_single(*args,**kwargs):
    _test1D_jac_separate(*args,ix=0,**kwargs)

if scipy_version[1]>5:
    def test1D_jac_approx_2point(*args,**kwargs):
        _test1D_jac_approx(*args,method='2-point',**kwargs)

    def test1D_jac_approx_3point(*args,**kwargs):
        _test1D_jac_approx(*args,method='3-point',**kwargs)

if scipy_version[1]>5 and scipy_version[1]<8:
    # There is a bug with using method cs in scipy>=1.8
    def test1D_jac_approx_cs(*args,**kwargs):
        _test1D_jac_approx(*args,method='cs',**kwargs)

# test out scenario where gradient is approximated (using FD)
def test1D_jac_approx(*args,**kwargs):
    _test1D_jac_approx(*args,**kwargs)

def test1D_jac_approx_bounds(*args,**kwargs):
    _test1D_jac_approx(*args,bounds=True,**kwargs)

def test1D_jac_approx_single(*args,**kwargs):
    _test1D_jac_approx(*args,ix=0,**kwargs)

# test out scenario where equality constraint is given 
def test1D_const_eq(*args,**kwargs):
    _test1D_const_eq(*args,**kwargs)

def test1D_const_eq_bounds(*args,**kwargs):
    _test1D_const_eq(*args,bounds=True,**kwargs)

def test1D_const_eq_single(*args,**kwargs):
    _test1D_const_eq(*args,ix=0,**kwargs)    

# test out scenario where inequality constraint is given 
def test1D_const_ineq(*args,**kwargs):
    _test1D_const_ineq(*args,**kwargs)

def test1D_const_ineq_bounds(*args,**kwargs):
    _test1D_const_ineq(*args,bounds=True,**kwargs)

def test1D_const_ineq_single(*args,**kwargs):
    _test1D_const_ineq(*args,ix=0,**kwargs)        

